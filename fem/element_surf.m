function [surf] = element_surf(ngp)
surf.xfem = false;
surf.crack = @crack;
surf.nne = 2;
surf.nsd = 1;
surf.area = @area;
surf.N    = @shape_functions;
surf.dN   = @shape_function_gradient;
[surf.gp,surf.gw] = gauss_points(ngp,1,'quad');


    % Calculate the length of the line.
    function [A] = area(X)
        A = abs(X(2)-X(1));
    end

    % Return the crack intersections. (Null)
    function [] = crack()
    end

    % Return the surf shape function evaluated at x, range {-1,1}.
    function [N] = shape_functions(x)
        N(1) = 0.5 * (1.0-x);
        N(2) = 0.5 * (1.0+x);
    end

    % Return the surf shape function gradient evaluated at x, range {-1,1}.
    function [dN] = shape_function_gradient(x)
        dN(1,1) = -0.5;
        dN(1,2) =  0.5;
    end
end

