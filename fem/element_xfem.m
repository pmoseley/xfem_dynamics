function [xfem] = element_xfem(quad,X,sdf,gp,gw,xic,intrBOOL)
% Only for splitting quads. Input parameters:
%   quad        = standard (non-XFEM) quadratic element.
%   X           = reference coordinates of the element nodes.
%   sdf         = signed distance function at the nodes.
%   gp,gw       = gauss integration points and weights.
%   xic         = sdf crack intersection points in local coordinates.
%   intrBOOL    = boolean, true if using interface elements.
xfem.xfem = true;
xfem.crack = @crack;
xfem.nne = 4;
xfem.nsd = 2;
xfem.area = @area;
xfem.N    = @shape_functions;
xfem.dN   = @shape_function_gradient;
xfem.gp   = gp;
xfem.gw   = gw;


% Calculate the area of a quadrilateral.
function [A] = area(X)
    A = quad.area(X);
end

% Return the crack intersections.
function [c] = crack()
    c = xic;
end

% Return the xfem shape function evaluated at x.
% Parameter 1 = x, parameter 2 = enrichedBOOL
function [N] = shape_functions(x,varargin)
    N = quad.N(x);
    if(size(varargin,2)==1 && varargin{1})
        N = N.*enrichment(N);
    end
end

% Return the xfem shape function gradient evaluated at x.
% Parameter 1 = x, parameter 2 = enrichedBOOL
function [dN] = shape_function_gradient(x,varargin)
    dN = quad.dN(x);
    if(size(varargin,2)==1 && varargin{1})
        for(i=1:size(dN,1))
            dN(i,:) = dN(i,:).*enrichment(quad.N(x));
        end
    end
end

% Enrichment function.
function [Y] = enrichment(Nx)
    if(~intrBOOL)
        % Shifted enrichment function.
        Y = Heaviside(Nx*sdf') - Heaviside(sdf);
        % Generalized Heaviside function. Equivalent, see Areias, P.M. and Belytschko, T.
        % A comment on the article "A finite element method for simulation of strong and weak
        % discontinuities in solid mechanics" by A. Hansbo. and P. Hansbo Commentary
        % Y = Generalized_Heaviside(Nx*sdf');
    else
        % Y = abs(Nx*sdf') - abs(sdf);      % From Farsad paper.
        % Y = abs(sdf) - abs(Nx*sdf');        % Possibly the correct Farsad form?
        Y = Nx*abs(sdf') - abs(Nx*sdf');    % Form from Yvonnet. Equivalent?
    end
end

% Step function H(x) = 0 when x<=0
%               H(x) = 1 when x>0
function [Y] = Heaviside(x)
    Y = x>0;
end

% Step function H(x) = -1 when x<=0
%               H(x) =  1 when x>0
function [Y] = Generalized_Heaviside(x)
    Y = 2*(x>0)-1;
end

end
