function [tri] = element_tri(ngp)
tri.xfem = false;
tri.crack = @crack;
tri.nne = 3;
tri.nsd = 2;
tri.area = @area;
tri.N    = @shape_functions;
tri.dN   = @shape_function_gradient;
[tri.gp,tri.gw] = gauss_points(ngp,2,'tri_xy');
% [tri.gp,tri.gw] = gauss_points(ngp,2,'tri');


% Calculate the area of a triangle.
function [A] = area(X)
    x1y2 = X(1,1)*X(2,2);   x2y1 = X(2,1)*X(1,2);
    x2y3 = X(2,1)*X(3,2);   x3y2 = X(3,1)*X(2,2);
    x3y1 = X(3,1)*X(1,2);   x1y3 = X(1,1)*X(3,2);
    A = 0.5*abs(x1y2+x2y3+x3y1 - x2y1-x3y2-x1y3);

% Return the crack intersections. (Null)
function [] = crack()

% Return the tri shape function evaluated at x.
function [N] = shape_functions(x)
    N(1) = x(1);
    N(2) = x(2);
    N(3) = 1-x(1)-x(2); % Shape functions in XY.
    % N(3) = x(3); % Shape functions in volume coords.

% Return the tri shape function gradient evaluated at x.
function [dN] = shape_function_gradient(x)
    dN(1,1) = 1; dN(1,2) = 0; dN(1,3) = -1;
    dN(2,1) = 0; dN(2,2) = 1; dN(2,3) = -1;

