function [quad] = element_quad(ngp)
quad.xfem = false;
quad.crack = @crack;
quad.nne = 4;
quad.nsd = 2;
quad.area = @area;
quad.N    = @shape_functions;
quad.dN   = @shape_function_gradient;
[quad.gp,quad.gw] = gauss_points(ngp,2,'quad');


% Calculate the area of a quadrilateral.
function [A] = area(X)
    x1y2 = X(1,1)*X(2,2);   x2y1 = X(2,1)*X(1,2);
    x2y3 = X(2,1)*X(3,2);   x3y2 = X(3,1)*X(2,2);
    x3y4 = X(3,1)*X(4,2);   x4y3 = X(4,1)*X(3,2);
    x4y1 = X(4,1)*X(1,2);   x1y4 = X(1,1)*X(4,2);
    A = 0.5*abs(x1y2+x2y3+x3y4+x4y1 - x2y1-x3y2-x4y3-x1y4);

% Return the crack intersections. (Null)
function [] = crack()

% Return the quad shape function evaluated at x.
function [N] = shape_functions(x)
    xm=0.25*(1.0-x(1)); xp=0.25*(1.0+x(1));
    ym=1.0-x(2);        yp=1.0+x(2);
    N(1) = xm*ym;
    N(2) = xp*ym;
    N(3) = xp*yp;
    N(4) = xm*yp;

% Return the quad shape function gradient evaluated at x.
function [dN] = shape_function_gradient(x)
    xm=0.25*(1.0-x(1)); xp=0.25*(1.0+x(1));
    ym=0.25*(1.0-x(2)); yp=0.25*(1.0+x(2));
    dN(1,1) = -ym;    dN(2,1) = -xm;
    dN(1,2) =  ym;    dN(2,2) = -xp;
    dN(1,3) =  yp;    dN(2,3) =  xp;
    dN(1,4) = -yp;    dN(2,4) =  xm;

