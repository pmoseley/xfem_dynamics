classdef engine_fem
    properties (Hidden)
        rho
        mu
        lambda
    end

    methods
        %----------------------------------------------------------------------
        % Constructor.
        %----------------------------------------------------------------------
        function FEM = engine_fem(E, nu, rho)
            FEM.rho    = rho;
            FEM.mu     = E/(2.0+2.0*nu);
            FEM.lambda = FEM.mu*nu/(0.5-nu);
        end

        %----------------------------------------------------------------------
        % Calculate the Second Piola-Kirchoff stress in plane strain.
        %----------------------------------------------------------------------
        function S = PK2(FEM,F)
            C = FEM.right_CG(F);
            pressure = 0.5*(C(1)+C(2)-2.0)*FEM.lambda;
            S(1) = FEM.mu*(C(1)-1.0) + pressure;
            S(2) = FEM.mu*(C(2)-1.0) + pressure;
            S(3) = FEM.mu*C(3);
        end

        %----------------------------------------------------------------------
        % Calculate the deformation gradient.
        %----------------------------------------------------------------------
        function [F,dNdX,dPdX] = deformation_gradient(FEM,domain,e,x)
            element = domain.element(e);
            elmX = domain.gather(domain.X,e);
            elmx = domain.gather(domain.x,e);
            invJ = inv(FEM.J(element,elmX,x));
            dNdX = invJ * element.dN(x);
            % For XFEM elements, F = F_cont + F_disc
            if(element.xfem)
                psi = domain.gather(domain.xfem.x, e);
                dPdX = invJ * element.dN(x,true);
                F = (dNdX * elmx)' + (dPdX * psi)';
            else
                dPdX = dNdX;    % (Unused, but needs to be returned)
                F = (dNdX * elmx)';
            end
        end

        %----------------------------------------------------------------------
        % Calculate the individual, fully consistent mass matrices.
        %----------------------------------------------------------------------
        function [MUU,MUQ,MQQ] = mass_matrices(FEM, domain, lump_non_xfem, lump_xfem)
            MUU = sparse(domain.nn,domain.nn);
            MUQ = sparse(domain.nn,domain.nn);
            MQQ = sparse(domain.nn,domain.nn);
            for(e=1:domain.ne)
                element = domain.element(e);
                X = domain.gather(domain.X,e);
                conn = domain.conn(e,:);
                for(q=1:length(element.gp))
                    detJ = det(FEM.J(element,X,element.gp(q,:)));
                    w = element.gw(q) * detJ * FEM.rho;
                    % FEM elements.
                    if(~element.xfem)
                        N = element.N(element.gp(q,:));
                        % Diagonalized FEM elements.
                        if(lump_non_xfem)
                            muu = w * N;
                            for(i=1:element.nne)
                                MUU(conn(i),conn(i)) = MUU(conn(i),conn(i)) + muu(i);
                            end
                        % Consistent FEM elements.
                        else
                            muu = w * (N'*N);
                            for(i=1:element.nne)
                                for(j=1:element.nne)
                                    MUU(conn(i),conn(j)) = MUU(conn(i),conn(j)) + muu(i,j);
                        end;end;end
                    % XFEM elements.
                    else
                        N = element.N(element.gp(q,:),false);
                        P = element.N(element.gp(q,:),true);
                        % Diagonalized XFEM elements.
                        if(lump_xfem)
                            % mI = sumJ sumQP(w * rho * NI * NJ), where sumJ(NJ) == 1.0
                            muu = w * N;
                            for(i=1:element.nne)
                                MUU(conn(i),conn(i)) = MUU(conn(i),conn(i)) + muu(i);
                            end
                        % Consistent XFEM elements.
                        else
                            muu = w * (N'*N);
                            for(i=1:element.nne)
                                for(j=1:element.nne)
                                    MUU(conn(i),conn(j)) = MUU(conn(i),conn(j)) + muu(i,j);
                        end;end;end
                        % For XFEM nodes, the row-sum mass must be explicitly summed
                        % mI = sumJ sumQP(w * rho * PI * PJ), where sumJ(PJ) != 1.0
                        muq = w * (N'*P);
                        mqq = w * (P'*P);
                        % Scatter.
                        for(i=1:element.nne)
                            for(j=1:element.nne)
                                MUQ(conn(i),conn(j)) = MUQ(conn(i),conn(j)) + muq(i,j);
                                MQQ(conn(i),conn(j)) = MQQ(conn(i),conn(j)) + mqq(i,j);
        end;end;end;end;end;end
    end

    methods (Static)
        %----------------------------------------------------------------------
        % Find the local coordinates of a point in global coordinates.
        %----------------------------------------------------------------------
        function xi = point_in_element(elm,elmX,x)
            % Start in the center of element.
            xi = [0,0];
            for(i=0:100)
                err = x - elm.N(xi)*elmX;
                if(dot(err,err) < 1.0e-12) break; end
                dx = err*inv(elm.dN(xi) * elmX);
                xi = xi+dx;
                if(max(abs(xi)) > 1.0+norm(dx)) break; end
            end
            err = x - elm.N(xi)*elmX;
            if(dot(err,err) > 1e-6)
                disp('Point-in-element didnt converge');
            end
            if(sum(isnan(xi)) > 0)
                disp('Point-in-element gave NaN');
            end
        end

        %----------------------------------------------------------------------
        % Return the Jacobian evaluated at x.
        %----------------------------------------------------------------------
        function Jacobian = J(elm,elmX,x)
            Jacobian = elm.dN(x) * elmX;
        end

        %----------------------------------------------------------------------
        % Construct the FE B0 matrix. When it's an XFEM element, pass dPdX.
        %----------------------------------------------------------------------
        function B = B0(elm,F,dNdX)
            for(I=1:elm.nne)
                B(1, I*elm.nsd-1) = dNdX(1,I)*F(1,1);
                B(1, I*elm.nsd)   = dNdX(1,I)*F(2,1);
                B(2, I*elm.nsd-1) = dNdX(2,I)*F(1,2);
                B(2, I*elm.nsd)   = dNdX(2,I)*F(2,2);
                B(3, I*elm.nsd-1) = dNdX(1,I)*F(1,2) + dNdX(2,I)*F(1,1);
                B(3, I*elm.nsd)   = dNdX(1,I)*F(2,2) + dNdX(2,I)*F(2,1);
            end
        end

        %----------------------------------------------------------------------
        % Calculate the right Cauchy-Green tensor.
        %----------------------------------------------------------------------
        function C = right_CG(F)
            C(1) = F(1,1)*F(1,1) + F(2,1)*F(2,1);
            C(2) = F(1,2)*F(1,2) + F(2,2)*F(2,2);
            C(3) = F(1,1)*F(1,2) + F(2,1)*F(2,2);
        end
    end
end

