function domain = xfem_implicit(varargin)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Author:   Philip Moseley
    %               Philip.Moseley@u.northwestern.edu
    %
    % Usage:    Calculates the static FE response to a system under load.
    %           Uses XFEM to model discontinuities in the system without
    %           remeshing.
    %
    % License:  Academic Free License v3.0. You may modify and distribute
    %           the code as you please, as long as you maintain the
    %           attribution notices.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    old_path = path; addpath(genpath('.'));

    %------------------------Input--------------------------
    %----- Material Options.
    E = 3.0e7;          % Elastic modulus.
    nu = 0.3;           % Poisson's ratio.
    D = E/(1-nu^2)*[1 nu 0; nu 1 0; 0 0 (1-nu)/2];                      % Plane stress.
    % D = E/((1+nu)*(1-2*nu))*[1-nu nu 0; nu 1-nu 0; 0 0 (1-2*nu )/2];    % Plane strain.

    %----- General Options.
    alpha = E * 1.0e8;  % Penalty factor for enforcing essential BCs.
    solver = 'bicg';    % bicg, bicgstab, or direct.
    fig.scale = 1.0e3;  % Factor to visibly scale the displaced mesh.
    fig.xfem = true;    % Plot the XFEM surfaces.
    fig.ref = true;     % Plot reference mesh underneath displaced mesh.

    %----- Abaqus mesh file.
    mesh.scale = 1.0e0;         % Scale the mesh for size-effect studies.
    mesh.file = './meshes/xfem_simple.inp';     % Simple xfem test, 16 element square.

    %----- Crack endpoints.
    % crack = [];                     % No crack.
    % crack = [-2.0, 0.0; 2.0, 0.0];  % Crack xfem_simple.inp down the middle.
    crack = [-2.0, 0.0; 0.0, 0.0];  % Crack xfem_simple.inp down the middle.

    %----- Boundary Conditions, one per row.
    % Use -inf or inf to indicate min or max coordinate of a dimension.
    %-- Coordinates of points fixed in both x and y.
    bc.fixed = [-2.0,-2.5];
    %-- Displacements in X or Y, [x coord, displ] or [y coord, displ]
    % bc.ux = [-inf, 0.0];
    bc.uy = [-inf, 0.0];
    %-- Forces in X or Y, [x coord, force] or [y coord, force]
    bc.fy = [inf, 20.0];

    %----- Integrator Options.
    ngp = 2;            % Number of gauss points per dimension.
    subint_tri = true;  % Whether or not to subintegrate using triangles.
    tri_ngp = 4;        % Number of gauss points (total) for triangles.

    

    %-----------------------Preprocessing-------------------------
    domain = create_domain(mesh,true,false,false);
    FEM  = engine_fem(E,nu,0.0);
    LS   = engine_ls();
    SINT = engine_quadrature(FEM,ngp,subint_tri,tri_ngp,1);
    domain.sdf = LS.sdf(crack,domain);      % Calculate the sdf at every node.
    domain = SINT.quadpoints(domain);       % Calculate the new quadpoints.

    % Create the output figure.
    figure(); title('XFEM Implicit Solution'); fig.axis = gca; fig.handle = gcf;



    %----------------------Calculate------------------------
    % Develop the element stiffness and force matrices.
    for(e=1:domain.ne)
        element = domain.element(e);
        X = domain.gather(domain.X,e);
        ke = zeros(2*domain.nne);
        % fe = zeros(2*domain.nne,1);   % TODO? - body forces.
        if(element.xfem)
            kuq= zeros(2*domain.nne); kqq= zeros(2*domain.nne);
        end

        for(q=1:length(element.gp))
            x = element.gp(q,:);
            % This works because the current config == reference config, so F == I.
            % See Nonlinear FE for Continua and Structures page 203.
            [F,dNdX,dPdX] = FEM.deformation_gradient(domain,e,x);
            B = FEM.B0(element,F,dNdX);
            detJ = det(FEM.J(element,X,x));
            ke = ke + element.gw(q) * detJ * (B'*D*B);

            if(element.xfem)
                Beta = FEM.B0(element,F,dPdX);
                kuq = kuq + element.gw(q) * detJ * (B'*D*Beta);
                kqq = kqq + element.gw(q) * detJ * (Beta'*D*Beta);
            end
        end

        % Scatter into the full matrices.
        domain.K = domain.scatter(domain.K,ke,e);
        % domain.f = domain.scatter(domain.f,fe,e); % TODO? - body forces.
        if(element.xfem)
            domain.Kuq = domain.scatter(domain.Kuq,kuq,e);
            domain.Kqq = domain.scatter(domain.Kqq,kqq,e);
        end
    end

    % Apply the boundary conditions.
    domain = apply_bcs(domain,bc,fig,false,alpha);

    % Build and solve the system of equations.
    [domain,idxs] = strip_matrix(domain);
    domain.K = [domain.K domain.Kuq; domain.Kuq' domain.Kqq];
    domain.f = [domain.f; domain.xfem.q];
    domain.x = implicit_solver(domain.K,domain.f,solver);

    domain.xfem.x(idxs) = domain.x(domain.nn+1:end,:);
    domain.x = domain.X + domain.x(1:domain.nn,:);

    % Plot the results.
    plot_sparsity(domain);
    plot_disp_mesh(domain,bc,crack,fig);
    print_disp_magnitude(domain,fig);
    path(old_path);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper Functions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Create the minimal system of equations by stripping out empty rows/columns.
    % This allows us to use full-size matrices for easy indexing, but still solve
    % the smaller system. We save "idxs" in order to expand the results back to
    % the full matrices for indexing again.
    function [domain,idxs] = strip_matrix(domain)
        idxs = any(domain.Kuq);
        if(isequal(idxs,any(domain.Kqq),any(domain.Kqq,2)))
            error('Problem with stripping XFEM matrix.');
        end
        domain.Kuq = domain.Kuq(:,idxs);        % Extract cols with a non-0.
        domain.Kqq = domain.Kqq(:,idxs);        % Extract cols with a non-0.
        domain.Kqq = domain.Kqq(idxs,:);        % Extract rows with a non-0.
        domain.xfem.q = domain.xfem.q(idxs);    % Extract rows with a non-0.
        idxs = reshape_voigt(idxs);
    end
end

