%% Plots xfem crack on displaced mesh.
function [] = plot_xfem(domain,crack,fig)
    if(~fig.xfem) return; end
    if(isempty(crack)) return; end    % Do nothing if there is no crack.
    hold(fig.axis,'on');

    % Calculate crack location in current coordinates.
    for(e=1:domain.ne)
        element = domain.element(e);
        if(element.xfem)
            xic = element.crack();
            x = domain.gather(domain.x,e);
            p = domain.gather(domain.xfem.x,e);
            % Determine if the intersections are on opposite or adjacent walls.
            if(sum(abs(diff(abs(xic)))<1e-6)) adjacent = false;
            else                              adjacent = true;
            end
            for(c=1:size(xic,1))
                % Delta shifts the point slightly above or slightly below the crack
                % so we can calculate the displacement field on both sides.
                delta = [0,0];
                if(abs(xic(c,1)) > abs(xic(c,2)))
                    if(adjacent && xic(c,1) < 0.0) delta(2) = -0.05;
                    else delta(2) = 0.05;
                    end
                else
                    if(adjacent && xic(c,2) < 0.0) delta(1) = -0.05;
                    else delta(1) = 0.05;
                    end
                end
                tempx1 = xic(c,:)+delta;
                tempx2 = xic(c,:)-delta;

                edge1(c,:) = element.N(tempx1)*x + element.N(tempx1,true)*p;
                edge2(c,:) = element.N(tempx2)*x + element.N(tempx2,true)*p;
            end
            % Plot the crack.
            plot(fig.axis,edge1(:,1),edge1(:,2),'bx--','LineWidth',2);
            plot(fig.axis,edge2(:,1),edge2(:,2),'bx--','LineWidth',2);
        end
    end

    % Plot the reference crack.
    if(fig.ref)
        for(c=1:2:size(crack))
            plot(fig.axis,crack(c:c+1,1),crack(c:c+1,2),'rx--');
        end
    end

    hold(fig.axis,'off');

