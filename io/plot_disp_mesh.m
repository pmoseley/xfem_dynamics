%% Plots Elements on a displaced mesh.
function [] = plot_disp_mesh(domain,bc,crack,fig)
    cla(fig.axis);
    set(fig.handle,'CurrentAxes',fig.axis);
    set(fig.handle, 'Renderer', 'painters');

    % Scale the domain.
    domain.x = domain.X + ((domain.x - domain.X) * fig.scale);
    domain.xfem.x = domain.xfem.x * fig.scale;

    % Plot the reference mesh.
    hold(fig.axis,'on');
    set(0,'CurrentFigure',fig.handle);
    if(fig.ref)
        patch('Faces', domain.conn, 'Vertices', domain.X, 'EdgeColor', [1 0 0], ...
              'FaceColor', 'none', 'LineWidth', 1, 'DisplayName', 'Original');
    end
    % Plot the displaced mesh.
    patch('Faces', domain.conn, 'Vertices', domain.x, 'EdgeColor', [0 0 0], ...
          'FaceColor', 'none', 'LineWidth', 2.0, 'DisplayName', 'Displaced');
    alpha(0.5);
    hold(fig.axis,'off');

    % Plot the crack.
    if(fig.xfem) plot_xfem(domain,crack,fig); end
    % Plot the BCs.
    apply_bcs(domain,bc,fig,true,0.0);

    % Set the boundary of the figure.
    all_nds = [domain.X; domain.x];
	x0=min(all_nds(:,1)); x1=max(all_nds(:,1)); dx=range(all_nds(:,1));
    y0=min(all_nds(:,2)); y1=max(all_nds(:,2)); dy=range(all_nds(:,2));
    wx=0.1*dx; wy=0.1*dy;
    axis(fig.axis,[x0-wx, x1+wx, y0-wy, y1+wy]);
    axis(fig.axis,'equal');

    % Create the legend for the plot.
    if(fig.ref)
        if(fig.xfem)
            legend('Reference Mesh','Displaced Mesh','XFEM Surfaces','Location','NorthWest');
        else
            legend('Reference Mesh','Displaced Mesh','Location','NorthWest');
        end
    else
        if(fig.xfem)
            legend('Displaced Mesh','XFEM Surfaces','Location','NorthWest');
        else
            legend('Displaced Mesh','Location','NorthWest');
        end
    end
    legend('boxoff');
    pause(0.02);

