function [] = print_disp_magnitude(domain,fig)
% Print the order of magnitude for the displacements.
    u = domain.x - domain.X;
    disp_max = sqrt(max(u(:,1).*u(:,1) + u(:,2).*u(:,2)));
    fprintf('Order of magnitude for displacements: %f\n',disp_max);
    fprintf('Displacements in figure scaled by:    %f\n',fig.scale);
end

