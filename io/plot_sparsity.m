function [] = plot_sparsity(domain)
% Plot the sparsity patterns.
    figure;
    subplot(1,5,1:4); spy(domain.K);
    title('Sparsity Pattern for Stiffness and Force Matrices');
    subplot(1,5,5);   spy(domain.f);
end

