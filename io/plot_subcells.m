% Plot the cell and subcells.
function plot_subcells(domain,e,crack)
    X = domain.gather(domain.X,e);
    bounds = [min(X(:,1))-1;max(X(:,1))+1;
              min(X(:,2))-1;max(X(:,2))+1];
    % Ensure the endpoints of the crack are outside the element.
    cv = 20*(crack(2,:)-crack(1,:));
    crack(1,:) = crack(1,:) - cv;
    crack(2,:) = crack(2,:) + cv;
    % Convert gauss points to reference coordinates.
    gp = [];
    for(i=1:size(domain.element(e).gp,1))
        ip = domain.element(e).gp(i,:);
        p = domain.element(e).N(ip) * X;
        gp = [gp; p];
    end
    %----- Reference elements.
    figure(); axis square; hold on; axis(bounds);
    patch(X(:,1), X(:,2),'w')                   % Plot cell borders.
    plot(crack(:,1),crack(:,2),'rx-');          % Plot crack.
    plot(gp(:,1),gp(:,2),'kx','LineWidth',2);   % Plot gauss points.
    hold off;

