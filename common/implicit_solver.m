% Solve the implicit system using the solver of choice.
function [x] = solver(K,f,solver)
    disp('Solving implicit system of equations...');
    tol = [];
    maxit = 100000;
    switch(solver)
        case 'direct'
            x = (K\f);
        case 'bicg'
            x = bicg(K,f,tol,maxit);
        case 'bicgstab'
            x = bicgstab(K,f,tol,maxit);
        case 'bicgstabl'
            x = bicgstabl(K,f,tol,maxit);
        case 'cgs'
            x = cgs(K,f,tol,maxit);
        case 'gmres'
            x = gmres(K,f,10,tol,maxit);
        case 'minres'
            x = minres(K,f,tol,maxit);
        case 'compare'
            [x,flag,relres,iter,resvec] = bicg(K,f,tol,maxit);
            figure; semilogy(resvec/norm(f)); title('BICG');

            [x,flag,relres,iter,resvec] = bicgstab(K,f,tol,maxit);
            figure; semilogy(0.5*(1:length(resvec)),resvec/norm(f)); title('BICG Stabilized');

            [x,flag,relres,iter,resvec] = bicgstabl(K,f,tol,maxit);
            figure; semilogy(0.5*(1:length(resvec)),resvec/norm(f)); title('BICG Stabilized L');

            [x,flag,relres,iter,resvec] = cgs(K,f,tol,maxit);
            figure; semilogy(resvec/norm(f)); title('CGS');

            [x,flag,relres,iter,resvec] = gmres(K,f,10,tol,maxit/10);
            figure; semilogy(resvec/norm(f)); title('GMRES');

            [x,flag,relres,iter,resvec] = minres(K,f,tol,maxit);
            figure; semilogy(resvec/norm(f)); title('MINRES');
        otherwise
            error('Invalid choice of solver.');
    end
    x = reshape_voigt(x');
    disp('Solving complete.');
end
