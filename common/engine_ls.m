classdef engine_ls
    methods
        %----------------------------------------------------------------------
        % Constructor.
        %----------------------------------------------------------------------
        function LS = engine_ls()
        end

        %----------------------------------------------------------------------
        % Calculate the signed distance from a crack to all the nodes.
        % sdf.f = signed distance from the crack.
        % sdf.g = 1 if inside the crack segment, 0 if outside the crack segment.
        %----------------------------------------------------------------------
        function sd = sdf(LS,crack,domain)
            if(isempty(crack))
                sd.f = -ones(1,domain.nn);
                sd.g = -ones(1,domain.nn);
            else
                cv = diff(crack);
                for(c=1:size(crack,1)/2)
                    tip1_tangent = [crack(2*c-1,:); crack(2*c-1,:)+[-cv(2*c-1,2), cv(2*c-1,1)]];
                    tip2_tangent = [crack(2*c,:);   crack(2*c,  :)+[ cv(2*c-1,2),-cv(2*c-1,1)]];
                    for(n=1:domain.nn)
                        sd(c).f(n) = LS.signed_distance(crack(2*c-1:2*c,:), domain.X(n,:),true);
                        sd(c).g(n) = (LS.signed_distance(tip1_tangent, domain.X(n,:),false)>=0);
                        sd(c).g(n) = sd(c).g(n) * (LS.signed_distance(tip2_tangent, domain.X(n,:),false)>=0);
                    end
                end
            end
        end
    end

    methods (Access = private)
        %----------------------------------------------------------------------
        % Calculate the signed distance from a crack to a point x.
        %   segmentBOOL determines to calculate distance to a line or segment.
        %----------------------------------------------------------------------
        function sd = signed_distance(LS, crack, x, segmentBOOL)
            a = crack(1,:); b = crack(2,:);
            if(segmentBOOL) dist = LS.dist_segment(x, a, b);
            else            dist = LS.dist_line(x, a, b);
            end

            % Determine sign by testing if [ax x ab].z > 0.
            if((x(1)-a(1))*(b(2)-a(2))-(x(2)-a(2))*(b(1)-a(1))>0.0)
                sd = dist;
            else
                sd = -dist;
            end
        end
    end

    methods (Access = private, Static)
        %----------------------------------------------------------------------
        % Calculate the (unsigned) distance to a segment p0-p1 from point x.
        %----------------------------------------------------------------------
        function d = dist_segment(x, p0, p1)
            v = p1-p0; w = x-p0;
            c1=dot(w,v);
            c2=dot(v,v);
            if(c1<=0)
                d = sqrt(dot(w,w));
            elseif(c2<=c1)
                w = x-p1;
                d = sqrt(dot(w,w));
            else
                w = x-(p0+v*(c1/c2));
                d = sqrt(dot(w,w));
            end
        end

        %----------------------------------------------------------------------
        % Calculate the (unsigned) distance to a line p0-p1 from point x.
        %----------------------------------------------------------------------
        function d = dist_line(x, p0, p1)
            v = p1-p0; r = x-p0;
            c1=dot(r,v);
            c2=dot(v,v);
            r = x-(p0+v*(c1/c2));
            d = sqrt(dot(r,r));
        end
    end
end

