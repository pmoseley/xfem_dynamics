function [domain] = apply_bcs(domain,bc,fig,plotBOOL,alpha)
    %% Apply Boundary Conditions
    % NOTE - this does NOT apply boundary conditions on XFEM degrees of freedom!
    % If plotBOOL is true, then only plot the points, don't actually apply BCs.
    % Note that this function doesn't use tolerances, so BC coords must be exact.
    % Alpha is only used for applying implicit boundary conditions.
    if(plotBOOL) hold(fig.axis,'on'); end
    tol = domain.scale * 1.0e-5;

    if(~domain.implicit)
        domain = explicit_bcs(domain,bc,fig,plotBOOL);
    else
        domain = implicit_bcs(domain,bc,fig,plotBOOL,alpha);
    end

    if(plotBOOL) hold(fig.axis,'off'); end





    %==================== EXPLICIT BCS ====================
    function [domain] = explicit_bcs(domain,bc,fig,plotBOOL)
        % Defined x velocities.
        if(isfield(bc,'vx') && ~isempty(bc.vx))
            for(i=1:domain.nn)
                for(j=1:size(bc.vx(:,1),1))
                    % Use -inf/inf to indicate left/right edge.
                    if(bc.vx(j,1) == -inf) bc.vx(j,1) = min(domain.X(:,1)); end
                    if(bc.vx(j,1) ==  inf) bc.vx(j,1) = max(domain.X(:,1)); end
                    % Apply the BC.
                    if(abs(domain.X(i,1)-bc.vx(j,1)) < tol)
                        if(plotBOOL)
                            plot(fig.axis,domain.X(i,1),domain.X(i,2),'>','MarkerFaceColor','b','MarkerSize',5);
                        else
                            domain.v(i,1) = bc.vx(j,2);
                        end
        end; end; end; end;
        % Defined y velocities.
        if(isfield(bc,'vy') && ~isempty(bc.vy))
            for(i=1:domain.nn)
                for(j=1:size(bc.vy(:,1),1))
                    % Use -inf/inf to indicate bottom/top edge.
                    if(bc.vy(j,1) == -inf) bc.vy(j,1) = min(domain.X(:,2)); end
                    if(bc.vy(j,1) ==  inf) bc.vy(j,1) = max(domain.X(:,2)); end
                    % Apply the BC.
                    if(abs(domain.X(i,2)-bc.vy(j,1)) < tol)
                        if(plotBOOL)
                            plot(fig.axis,domain.X(i,1),domain.X(i,2),'^','MarkerFaceColor','b','MarkerSize',5);
                        else
                            domain.v(i,2) = bc.vy(j,2);
                        end
        end; end; end; end;
    end

    %==================== IMPLICIT BCS ====================
    function [domain] = implicit_bcs(domain,bc,fig,plotBOOL,alpha)
        %-------------------- Essential BCs, penalty method (displacements) --------------------
        % Points fixed in x and y.
        if(isfield(bc,'fixed') && ~isempty(bc.fixed))
            for(i=1:domain.nn)
                for(j=1:size(bc.fixed(:,1),1))
                    if(abs(domain.X(i,:)-bc.fixed(j,:)) < tol)
                        if(plotBOOL)
                            plot(fig.axis,domain.X(i,1),domain.X(i,2),'x','MarkerSize',12);
                        else
                            for(k=[i*2-1 i*2])
                                domain.K(k,k) = domain.K(k,k)+alpha;
                                % Don't need to apply to f because displ = 0.
                        end
        end; end; end; end; end;
        % Displacement in x.
        if(isfield(bc,'ux') && ~isempty(bc.ux))
            for(i=1:domain.nn)
                for(j=1:size(bc.ux(:,1),1))
                    % Use -inf/inf to indicate left/right edge.
                    if(bc.ux(j,1) == -inf) bc.ux(j,1) = min(domain.X(:,1)); end
                    if(bc.ux(j,1) ==  inf) bc.ux(j,1) = max(domain.X(:,1)); end
                    % Apply the BC.
                    if(abs(domain.X(i,1)-bc.ux(j,1)) < tol)
                        if(plotBOOL)
                            plot(fig.axis,domain.X(i,1),domain.X(i,2),'>','MarkerFaceColor','b','MarkerSize',5);
                        else
                            k = i*2-1;
                            domain.K(k,k) = domain.K(k,k)+alpha;
                            domain.f(k)=domain.f(k)+alpha*bc.ux(j,2);
                        end
        end; end; end; end;
        % Displacement in y.
        if(isfield(bc,'uy') && ~isempty(bc.uy))
            for(i=1:domain.nn)
                for(j=1:size(bc.uy(:,1),1))
                    % Use -inf/inf to indicate bottom/top edge.
                    if(bc.uy(j,1) == -inf) bc.uy(j,1) = min(domain.X(:,2)); end
                    if(bc.uy(j,1) ==  inf) bc.uy(j,1) = max(domain.X(:,2)); end
                    % Apply the BC.
                    if(abs(domain.X(i,2)-bc.uy(j,1)) < tol)
                        if(plotBOOL)
                            plot(fig.axis,domain.X(i,1),domain.X(i,2),'^','MarkerFaceColor','b','MarkerSize',5);
                        else
                            k = i*2;
                            domain.K(k,k) = domain.K(k,k)+alpha;
                            domain.f(k) = domain.f(k)+alpha*bc.uy(j,2);
                        end
        end; end; end; end;

        %-------------------- Natural BCs (forces) --------------------
        % Forces in x.
        if(isfield(bc,'fx') && ~isempty(bc.fx))
            for(i=1:domain.nn)
                for(j=1:size(bc.fx(:,1),1))
                    % Use -inf/inf to indicate left/right edge.
                    if(bc.fx(j,1) == -inf) bc.fx(j,1) = min(domain.X(:,1)); end
                    if(bc.fx(j,1) ==  inf) bc.fx(j,1) = max(domain.X(:,1)); end
                    % Apply the BC.
                    if(abs(domain.X(i,1)-bc.fx(j,1)) < tol)
                        if(plotBOOL)
                            plot(fig.axis,domain.X(i,1),domain.X(i,2),'>g','MarkerFaceColor','g','MarkerSize',5);
                        else
                            domain.f(i*2-1) = domain.f(i*2-1)+bc.fx(j,2);
                        end
        end; end; end; end;
        % Forces in y.
        if(isfield(bc,'fy') && ~isempty(bc.fy))
            for(i=1:domain.nn)
                for(j=1:size(bc.fy(:,1),1))
                    % Use -inf/inf to indicate bottom/top edge.
                    if(bc.fy(j,1) == -inf) bc.fy(j,1) = min(domain.X(:,2)); end
                    if(bc.fy(j,1) ==  inf) bc.fy(j,1) = max(domain.X(:,2)); end
                    % Apply the BC.
                    if(abs(domain.X(i,2) == bc.fy(j,1)) < tol)
                        if(plotBOOL)
                            plot(fig.axis,domain.X(i,1),domain.X(i,2),'^g','MarkerFaceColor','g','MarkerSize',5);
                        else
                            domain.f(i*2) = domain.f(i*2)+bc.fy(j,2);
                        end
        end; end; end; end;
    end

end

