classdef create_domain
    properties
        % BOOLEANS.
        implicit        % Implicit or Explicit calculations. BOOL
        surfaces        % Include surface effects or no. BOOL
        interfaces      % Include interface effects or no. BOOL

        % COMMON VARIABLES.
        conn            % Connectivity matrix. [ne x nne]
        nn              % Number of nodes. INT
        ne              % Number of elements. INT
        nne             % Number of nodes per element. INT
        X               % Reference coordinates. [nn x dim]
        x               % Current coordinates. [nn x dim]
        sdf             % Signed distance function to nodes.
                        %   sdf(c).f = signed distance from crack c
                        %   sdf(c).g = 1 if inside crack segment, 0 if outside.
        element         % Element objects. element(e) = element object for element e.
        scale           % Scaling factor for the mesh.

        % IMPLICIT VARIABLES.
        f               % Global force vector.
        K               % Global stiffness matrix.
        Kuq             % XFEM stiffness matrix.
        Kqq             % XFEM stiffness matrix.

        % EXPLICIT VARIABLES.
        inv_mass        % Inverse nodal mass matrix.
        v               % Nodal velocities.
        a0              % Accelerations t=n.
        a1              % Accelerations t=n+1.

        % STRUCTURES.
        xfem
        surf
        intr
    end

    methods
        %----------------------------------------------------------------------
        % Constructors.
        %   Default constructor: create_domain(mesh,implicitBOOL,surfacesBOOL,interfacesBOOL)
        %   Second constructor:  create_domain(X,conn,implicitBOOL,surfacesBOOL,interfacesBOOL)
        %----------------------------------------------------------------------
        function [domain] = create_domain(varargin)
            if(nargin==4)
                domain = domain.read_mesh(varargin{1});
                domain.implicit = varargin{2};
                domain.surfaces = varargin{3};
                domain.interfaces = varargin{4};
            elseif(nargin==5)
                domain.X = varargin{1};
                domain.conn = varargin{2};
                domain.implicit = varargin{3};
                domain.surfaces = varargin{4};
                domain.interfaces = varargin{5};
                domain.scale = 1.0;
            else
                error('Incorrect number of arguments to create_domain');
            end
            domain.x  = domain.X;
            domain.nn = size(domain.X,1);
            domain.ne = size(domain.conn,1);
            domain.nne = size(domain.conn,2);
            fprintf('\tNodes: %d\n\tElements: %d\n',domain.nn,domain.ne);

            if(domain.implicit)   domain = domain.init_implicit(); end
            if(~domain.implicit)  domain = domain.init_explicit(); end
            if(domain.surfaces)   domain.surf = domain.init_surface(); end
            if(domain.interfaces) domain.intr = domain.init_interface(); end
        end


        %----------------------------------------------------------------------
        % Get local values from a matrix or vector using the connectivity.
        %----------------------------------------------------------------------
        function [L] = gather(domain,DATA,e)
            L = domain.gatherGEN(domain.conn,DATA,e);
        end
        function [L] = gatherS(domain,DATA,e)
            L = domain.gatherGEN(domain.surf.conn,DATA,e);
        end
        function [L] = gatherI(domain,DATA,e)
            L = domain.gatherGEN(domain.intr.conn,DATA,e);
        end

        %----------------------------------------------------------------------
        % Set local values from a matrix or vector using the connectivity.
        %----------------------------------------------------------------------
        function [DATA] = scatter(domain,DATA,L,e)
            DATA = domain.scatterGEN(domain.conn,DATA,L,e);
        end
        function [DATA] = scatterS(domain,DATA,L,e)
            DATA = domain.scatterGEN(domain.surf.conn,DATA,L,e);
        end
        function [DATA] = scatterI(domain,DATA,L,e)
            DATA = domain.scatterGEN(domain.intr.conn,DATA,L,e);
        end

        %----------------------------------------------------------------------
        % Add a new interface element.
        %----------------------------------------------------------------------
        function [intr] = add_interface(domain,X,elm,e)
            intr = domain.intr;
            intr.ne = intr.ne + 1;
            intr.element(intr.ne) = elm;
            nn = (intr.ne-1) * intr.nne;
            intr.conn = [intr.conn; nn+1:nn+intr.nne];
            intr.X = [intr.X; X];
            intr.map{e} = intr.ne;
            intr.rmap(intr.ne) = e;
        end
    end

    methods (Access=private)
        %----------------------------------------------------------------------
        % Get local values from a matrix or vector using the connectivity. Generic.
        %----------------------------------------------------------------------
        function [L] = gatherGEN(domain,conn,DATA,e)
            if(size(DATA,1) == 1) L = DATA(conn(e,:));
            else                  L = DATA(conn(e,:),:);
            end
        end

        %----------------------------------------------------------------------
        % Set local values from a matrix or vector using the connectivity. Generic.
        %----------------------------------------------------------------------
        function [DATA] = scatterGEN(domain,conn,DATA,L,e)
            if(size(DATA,1) == 1)       DATA(conn(e,:))   = DATA(conn(e,:)) + L;
            elseif(size(DATA,2) == 2)   DATA(conn(e,:),:) = DATA(conn(e,:),:) + L;
            else
                I = zeros(length(L),1);
                for(i=2:2:2*length(conn(e,:)))
                    I(i-1) = conn(e,i/2)*2-1;
                    I(i)   = conn(e,i/2)*2;
                end
                if(size(DATA,1) == size(DATA,2)) DATA(I,I) = DATA(I,I) + L;
                else                             DATA(I)   = DATA(I) + L;
                end
            end
        end

        %----------------------------------------------------------------------
        % Read in an ABAQUS .inp mesh file.
        %----------------------------------------------------------------------
        function [domain] = read_mesh(domain,mesh)
            % Import reference coordinates X.
            data = importdata(mesh.file);
            domain.X = data.('data');
            domain.X = domain.X(:,2:end);
            domain.X = domain.X * mesh.scale;

            % Import connectivity conn.
            retry = true;
            nlines = size(domain.X,1) + size(data.textdata,1);
            while(retry)
                data = importdata(mesh.file,',',nlines);
                retry = ~isfield(data,'data');
                nlines = nlines+1;
            end
            domain.conn = data.('data');
            domain.conn = domain.conn(:,2:end);
            domain.scale = mesh.scale;
            fprintf('Read in mesh: %s\n\tScaling factor: %f\n',mesh.file,mesh.scale);
        end

        %----------------------------------------------------------------------
        % Initialize variables for an implicit run.
        %----------------------------------------------------------------------
        function [domain] = init_implicit(domain)
            domain.f   = sparse(2*domain.nn,1);
            domain.K   = sparse(2*domain.nn,2*domain.nn);
            domain.Kuq = sparse(2*domain.nn,2*domain.nn);
            domain.Kqq = sparse(2*domain.nn,2*domain.nn);
            domain.xfem.x  = sparse(domain.nn,2);           % XFEM positions.
            domain.xfem.q  = sparse(2*domain.nn,1);         % XFEM force vector.
        end

        %----------------------------------------------------------------------
        % Initialize variables for an implicit run.
        %----------------------------------------------------------------------
        function [domain] = init_explicit(domain)
            domain.v  = zeros(size(domain.x));
            domain.a0 = zeros(size(domain.x));
            domain.a1 = zeros(size(domain.x));
            domain.xfem.x  = zeros(domain.nn,2); domain.xfem.v  = zeros(domain.nn,2);
            domain.xfem.a0 = zeros(domain.nn,2); domain.xfem.a1 = zeros(domain.nn,2);
        end

        %----------------------------------------------------------------------
        % Initialize variables for a run with surfaces.
        % Direction is correct, following the order of volume element nodes.
        % domain.surf.map relates a domain.element to the domain.surf.element
        % domain.surf.rmap relates the domain.surf.element to the domain.element
        %----------------------------------------------------------------------
        function [surf] = init_surface(domain)
            surf.ne = 0;
            surf.nne = 2;
            surf.conn = [];
            surf.map = {};
            surf.rmap = [];
            for(r=1:size(domain.conn,1))
                for(c=1:domain.nne)
                    n = domain.conn(r,[c mod(c,domain.nne)+1]);
                    ne = sum(sum(domain.conn==n(1)|domain.conn==n(2),2)==2);
                    if(ne == 1)
                        surf.conn = [surf.conn; n];
                    elseif(ne ~= 2)
                        error('Problem finding surface element connectivity.');
                    end
                end
                if(size(surf.conn,1) ~= surf.ne)
                    surf.map{r} = surf.ne+1:size(surf.conn,1);
                    surf.rmap(surf.ne+1:size(surf.conn,1)) = r;
                    surf.ne = size(surf.conn,1);
                else
                    surf.map{r} = [];
                end
            end
            fprintf('\tSurface Elements: %d\n',surf.ne);
        end

        %----------------------------------------------------------------------
        % Initialize variables for a run with interfaces.
        %----------------------------------------------------------------------
        function [intr] = init_interface(domain)
            intr.ne = 0;                % Number of interface elements.
            intr.nne = 2;               % Number of interface nodes per element.
            intr.conn = [];             % Interface connectivity.
            intr.X = [];                % Interface X.
            intr.map{domain.ne} = [];   % element -> surface mapping.
            intr.rmap = [];             % surface -> element mapping.
        end
    end
end
