% Reshape the voigt notation f into a standard matrix.
function [f] = reshape_voigt(f)
    f = [f(1:2:end);
    f(2:2:end)]';
end
