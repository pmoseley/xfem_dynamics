function varargout = gui(varargin)
    % GUI M-file for gui.fig
    %      GUI, by itself, creates a new GUI or raises the existing
    %      singleton*.
    %
    %      H = GUI returns the handle to a new GUI or the handle to
    %      the existing singleton*.
    %
    %      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in GUI.M with the given input arguments.
    %
    %      GUI('Property','Value',...) creates a new GUI or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before gui_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to gui_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES

    % Edit the above text to modify the response to help gui

    % Last Modified by GUIDE v2.5 20-Apr-2011 00:49:21

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @gui_OpeningFcn, ...
                       'gui_OutputFcn',  @gui_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT


    % --- Executes just before gui is made visible.
    function gui_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to gui (see VARARGIN)

    % Choose default command line output for gui
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);

    % UIWAIT makes gui wait for user response (see UIRESUME)
    % uiwait(handles.figure1);


    % --- Outputs from this function are returned to the command line.
    function varargout = gui_OutputFcn(hObject, eventdata, handles) 
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % handles    structure with handles and user data (see GUIDATA)

    % Get default command line output from handles structure
    varargout{1} = handles.output;


    % --- Executes during object creation, after setting all properties.
    function E_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to E (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



    % --- Executes during object creation, after setting all properties.
    function nu_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to nu (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



    % --- Executes during object creation, after setting all properties.
    function rho_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to rho (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    function refmesh_Callback(hObject, eventdata, handles)
    function subint_tri_Callback(hObject, eventdata, handles)
    function tint_type_Callback(hObject, eventdata, handles)
    function lump_xfem_Callback(hObject, eventdata, handles)
    function lump_non_xfem_Callback(hObject, eventdata, handles)
    function ignore_muq_Callback(hObject, eventdata, handles)

    % --- Executes on button press in compute.
    function compute_Callback(hObject, eventdata, handles)
    % hObject    handle to compute (see GCBO)
    % handles    structure with handles and user data (see GUIDATA)

    % Read the data from all the input fields.
    % Mesh file.
    ui.mesh.file = get(handles.mesh,'String');
    ui.mesh.scale = 1.0;
    % General options.
    ui.E = str2num(get(handles.E,'String'));
    ui.nu = str2num(get(handles.nu,'String'));
    ui.rho = str2num(get(handles.rho,'String'));
    ui.steps = str2num(get(handles.steps,'String'));
    ui.os = str2num(get(handles.os,'String'));
    ui.fig.ref = get(handles.refmesh,'Value');
    ui.fig.xfem = true;
    ui.fig.scale = 1.0;
    % Integrator options.
    ui.dt = str2num(get(handles.timestep,'String'));
    ui.ngp = str2num(get(handles.ngp,'String'));
    ui.tint_type = get(handles.tint_type,'Value')-1;
    ui.subint_tri = get(handles.subint_tri,'Value');
    ui.tri_ngp = str2num(get(handles.tri_ngp,'String'));
    % Mass matrix options.
    ui.mass_type = get(handles.mass_type,'Value')-1;
    ui.lump_non_xfem = get(handles.lump_non_xfem,'Value');
    ui.lump_xfem = get(handles.lump_xfem,'Value');
    ui.ignore_muq = get(handles.ignore_muq,'Value');
    % Boundary conditions.
    ui.bc.vx = [];
    if(get(handles.bc_vx_left_bool,'Value'))
        ui.bc.vx(1,1) = -inf;
        ui.bc.vx(1,2) = str2num(get(handles.bc_vx_left,'String'));
    end
    if(get(handles.bc_vx_right_bool,'Value'))
        ui.bc.vx(end+1,1) = inf;
        ui.bc.vx(end,2) = str2num(get(handles.bc_vx_right,'String'));
    end
    ui.bc.vy = [];
    if(get(handles.bc_vy_bottom_bool,'Value'))
        ui.bc.vy(1,1) = -inf;
        ui.bc.vy(1,2) = str2num(get(handles.bc_vy_bottom,'String'));
    end
    if(get(handles.bc_vy_top_bool,'Value'))
        ui.bc.vy(end+1,1) = inf;
        ui.bc.vy(end,2) = str2num(get(handles.bc_vy_top,'String'));
    end
    % Crack endpoints.
    ui.crack = eval(get(handles.crack,'String'));

    % Run the code.
    % set(hObject,'Enable','off');
    set(hObject,'String','Computing...');
    xfem_explicit(ui);
    set(hObject,'String','Compute');
    % set(hObject,'Enable','on');
    guidata(hObject, handles);


    % --- Executes during object creation, after setting all properties.
    function steps_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to steps (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



    % --- Executes during object creation, after setting all properties.
    function os_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to os (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function tri_ngp_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to tri_ngp (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function ngp_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to ngp (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function timestep_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to timestep (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function tint_type_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to tint_type (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes on selection change in mass_type.
    function mass_type_Callback(hObject, eventdata, handles)
    % hObject    handle to mass_type (see GCBO)
    % handles    structure with handles and user data (see GUIDATA)
    mass_type = get(hObject,'Value')-1;
    switch mass_type
        case 0
            set(handles.lump_non_xfem,'Enable','off');
            set(handles.lump_xfem,'Enable','off');
            set(handles.ignore_muq,'Enable','on');
        case 1
            set(handles.lump_non_xfem,'Enable','on');
            set(handles.lump_xfem,'Enable','on');
            set(handles.ignore_muq,'Enable','off');
        case 2
            set(handles.lump_non_xfem,'Enable','off');
            set(handles.lump_xfem,'Enable','off');
            set(handles.ignore_muq,'Enable','off');
    end
    guidata(hObject, handles);


    % --- Executes during object creation, after setting all properties.
    function mass_type_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to mass_type (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function mesh_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to mesh (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function listbox3_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to listbox3 (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



    % --- Executes during object creation, after setting all properties.
    function crack_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to crack (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function bc_vx_left_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bc_vx_left (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



    % --- Executes during object creation, after setting all properties.
    function bc_vy_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bc_vy (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes on button press in meshbutton.
    function meshbutton_Callback(hObject, eventdata, handles)
    % hObject    handle to meshbutton (see GCBO)
    % handles    structure with handles and user data (see GUIDATA)
    [path,file,ext] = fileparts(get(handles.mesh,'String'));
    [file,path,filter] = uigetfile({'*.inp','ABAQUS Mesh Files'},'Select a mesh file',path);
    if(~isequal(file,0))
        set(handles.mesh,'String',strcat(path,file));
    end


    % --- Executes during object creation, after setting all properties.
    function edit22_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bc_vy (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function edit21_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bc_vx_left (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



    % --- Executes during object creation, after setting all properties.
    function bc_vy_bottom_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bc_vy_bottom (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end



    % --- Executes during object creation, after setting all properties.
    function bc_vy_top_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bc_vy_top (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes on button press in bc_vy_bottom_bool.
    function bc_vy_bottom_bool_Callback(hObject, eventdata, handles)
    % hObject    handle to bc_vy_bottom_bool (see GCBO)
    % handles    structure with handles and user data (see GUIDATA)
    if(get(hObject,'Value'))
        set(handles.bc_vy_bottom_text,'Enable','on');
        set(handles.bc_vy_bottom,'Enable','on');
    else
        set(handles.bc_vy_bottom_text,'Enable','off');
        set(handles.bc_vy_bottom,'Enable','off');
    end
    guidata(hObject, handles);


    % --- Executes on button press in bc_vy_top_bool.
    function bc_vy_top_bool_Callback(hObject, eventdata, handles)
    % hObject    handle to bc_vy_top_bool (see GCBO)
    % handles    structure with handles and user data (see GUIDATA)
    if(get(hObject,'Value'))
        set(handles.bc_vy_top_text,'Enable','on');
        set(handles.bc_vy_top,'Enable','on');
    else
        set(handles.bc_vy_top_text,'Enable','off');
        set(handles.bc_vy_top,'Enable','off');
    end
    guidata(hObject, handles);



    % --- Executes during object creation, after setting all properties.
    function bc_vx_right_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bc_vx_right (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function figure1_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to figure1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called


    % --- Executes on button press in bc_vx_left_bool.
    function bc_vx_left_Callback(hObject, eventdata, handles)
    % hObject    handle to bc_vy_bottom (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

    % --- Executes on button press in bc_vx_left_bool.
    function bc_vx_left_bool_Callback(hObject, eventdata, handles)
    % hObject    handle to bc_vx_left_bool (see GCBO)
    % handles    structure with handles and user data (see GUIDATA)
    if(get(hObject,'Value'))
        set(handles.bc_vx_left_text,'Enable','on');
        set(handles.bc_vx_left,'Enable','on');
    else
        set(handles.bc_vx_left_text,'Enable','off');
        set(handles.bc_vx_left,'Enable','off');
    end
    guidata(hObject, handles);


    % --- Executes on button press in bc_vx_right_bool.
    function bc_vx_right_Callback(hObject, eventdata, handles)
    % hObject    handle to bc_vy_bottom (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

    % --- Executes on button press in bc_vx_right_bool.
    function bc_vx_right_bool_Callback(hObject, eventdata, handles)
    % hObject    handle to bc_vx_right_bool (see GCBO)
    % handles    structure with handles and user data (see GUIDATA)
    if(get(hObject,'Value'))
        set(handles.bc_vx_right_text,'Enable','on');
        set(handles.bc_vx_right,'Enable','on');
    else
        set(handles.bc_vx_right_text,'Enable','off');
        set(handles.bc_vx_right,'Enable','off');
    end
    guidata(hObject, handles);


    % --- Executes during object creation, after setting all properties.
    function edit27_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bc_vx_left (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


    % --- Executes during object creation, after setting all properties.
    function edit26_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to bc_vx_right (see GCBO)
    % handles    empty - handles not created until after all CreateFcns called
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
