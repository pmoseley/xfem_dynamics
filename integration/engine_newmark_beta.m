classdef engine_newmark_beta
    properties (Hidden)
        beta = 0.25
    end
    methods
        % Constructor.
        function [TINT] = engine_newmark_beta
            % When beta=0.5, this is equivalent to velocity verlet.
            fprintf('Using Newmark-Beta time integrator, beta=%f.\n',TINT.beta);
        end

        % Advances the position by integrating velocity and acceleration.
        function domain = pre(TINT,domain,dt)
            domain.x = domain.x + dt.*(domain.v + dt.*((0.5-TINT.beta).*domain.a0+TINT.beta.*domain.a1));
        end
    end

    methods (Static)
        % Advances the velocity by trapezoidal integration of acceleration.
        function domain = post(domain,dt)
            domain.v = domain.v + (0.5*dt).*(domain.a0 + domain.a1);
            domain.a0 = domain.a1;
            domain.a1 = zeros(size(domain.a1));
        end

    end
end

