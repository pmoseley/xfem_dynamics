function [gp,gw] = gauss_points(n,dimension,type)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Program:    Multi-dimensional gauss points calculator with weight
%               functions
%   Author:     Brent Lewis rocketlion@gmail.com
%   Modified by:Philip Moseley philip.moseley@u.northwestern.edu
%   History:    Originally written: 10/30/2006
%               Revision for symbolic logic:  1/2/2007
%               Fixed some bugs in triangles: 10/10/2010
%   Purpose:    Program calculates the gauss points for 1-D,2-D,3-D along
%               with their weights for use in numerical integration.  
%               Originally written for a Finite Element Program so has the
%               capability to give integration points for a 6-node Triangle
%               element
%   Input:      n:          Number of gauss points(Must be integer 0<n<22
%               dimension:  Dimension of space for gauss points(optional)
%               symbolic:   Logical statement for return values in symbolic
%                           form(optional)
%               type:       Type of finite element(optional)
%   Output:     gp:         Gauss points in either vector or matrix form
%                           depending on dimensions
%               gw:         Weighting values for Gaussian integration
%   Example:    [gp gw] = md_gauss(2)
%               Returns the gp = +/-sqrt(1/3) and gw = 1 1 which are the
%               gauss points and integration weights for 2 Gauss gp rule
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function [gp gw] = gauss_points(n,dimension,symbolic,type)
% if strcmp(upper(symbolic),'TRUE')
%     symbolic = 1;
% else
    symbolic = 0;
% end

% Multiple Levels of Input with default values
% if nargin == 1
%     dimension = 1;
%     type = 'QUAD4';
%     symbolic = 0;
% elseif nargin == 2
%     type = 'QUAD4';
%     symbolic = 0;
% elseif nargin == 3
%     type = 'QUAD4';
% end

% Error determination
if n < 1 || n > 21
    error('Number of gauss points must be 0<n<21.')     % Factorial only accurate to n = 21
elseif mod(n,1) ~= 0
    error('Points must be integer value')               % Check for non-integer points
elseif dimension < 1 || dimension > 3
    error('Dimension error:  0<Dimension<4')            % Dimension check
end

if strcmpi(type, 'QUAD')
    [gp,gw] = gp_quad(n,dimension);
elseif strcmpi(type, 'TRI')
    [gp,gw] = gp_tri(n);
elseif strcmpi(type, 'TRI_XY')
    [gp,gw] = gp_tri(n);
    X = [0,0; 1,0; 0,1];
    for(i=1:n) gp_xy(i,:) = gp(i,:)*X; end
    gp = gp_xy;
end

if symbolic == 1
    gp = sym(gp);
    gw = sym(gw);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Quadrilaterals.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [gp,gw] = gp_quad(n,dimension)
    syms x
    if n == 1
        point_1D = 0;
        c_1D = 2;
    else
        P = 1/(2^n*factorial(n))*diff((x^2-1)^n,n);  % Rodrigues' Formula
        point_1D = double(solve(P));
        % Weight Function described in Numerical Analysis book 8th edition-
        % Richard Burden page 223
        for i = 1 : length(point_1D)
            prod = 1;
            for j = 1 : length(point_1D)
                if i == j
                    continue
                else
                    prod = prod*(x-point_1D(j))/(point_1D(i)-point_1D(j));
                end
            end
            c_1D(i,1) = double(int(sym(prod),-1,1));
        end
    end

    if dimension == 1
        gp = point_1D;
        gw = c_1D;
    elseif dimension == 2
        k = 1;
        for i = 1:n
            for j = 1:n
                gp(k,:) = [point_1D(i),point_1D(j)];
                gw(k,1) = c_1D(i)*c_1D(j);
                k = k+1;
            end
        end
    elseif dimension == 3
        m = 1;
        for i = 1 : n
            for j = 1 : n
                for k = 1 : n
                    gp(m,:) = [point_1D(i),point_1D(j),point_1D(k)];
                    gw(m,1) = c_1D(i)*c_1D(j)*c_1D(k);
                    m = m+1;
                end
            end
        end
    end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Triangles.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [gp,gw] = gp_tri(n)
    if n == 1
        gp = ones(1,3)/3;
        gw = 1;
    elseif n == 3
        gp = ones(3)/6+eye(3)/2;
        gw = ones(3,1)/3;
    elseif n == 4
        gp = [ ...
            0.33333333333333333333, 0.33333333333333333333, 0.33333333333333333333; ...
            0.73333333333333333333, 0.13333333333333333333, 0.13333333333333333333; ...
            0.13333333333333333333, 0.73333333333333333333, 0.13333333333333333333; ...
            0.13333333333333333333, 0.13333333333333333333, 0.73333333333333333333 ];
        gw = [ ...
            -0.56250000000000000000, ...
            0.52083333333333333333, ...
            0.52083333333333333333, ...
            0.52083333333333333333];
    elseif n == 6
        g1 = (8-sqrt(10)+sqrt(38-44*sqrt(2/5)))/18;
        g2 = (8-sqrt(10)-sqrt(38-44*sqrt(2/5)))/18;
        gp = [ones(3)*g1+eye(3)*(1-3*g1);ones(3)*g2+eye(3)*(1-3*g2)];
        gw = [ ...
            0.22338158967801146570, ...
            0.22338158967801146570, ...
            0.22338158967801146570, ...
            0.10995174365532186764, ...
            0.10995174365532186764, ...
            0.10995174365532186764 ];
    elseif n == 7
        g1 = (6-sqrt(15))/21;
        g2 = (6+sqrt(15))/21;
        gp = [ones(3)*g1+eye(3)*(1-3*g1);ones(3)*g2+eye(3)*(1-3*g2);ones(1,3)/3];
        gw = [ones(3,1)*(155-sqrt(15))/1200;ones(3,1)*(155+sqrt(15))/1200;9/40];
    else
        error('Triangular gauss points must be 1,3,4,6,7.')
    end
    gw = gw./2;
