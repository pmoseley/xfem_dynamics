classdef engine_velocity_verlet
    methods
        % Constructor.
        function [TINT] = engine_velocity_verlet()
            disp('Using Velocity-Verlet time integrator.');
        end
    end

    methods (Static)
        % Advances the position by integrating velocity and acceleration.
        function domain = pre(domain,dt)
            domain.x = domain.x + dt.*(domain.v + (0.5*dt).*domain.a0);
        end

        % Advances the velocity by trapezoidal integration of acceleration.
        function domain = post(domain,dt)
            domain.v = domain.v + (0.5*dt).*(domain.a0 + domain.a1);
            domain.a0 = domain.a1;
            domain.a1 = zeros(size(domain.a1));
        end
    end
end

