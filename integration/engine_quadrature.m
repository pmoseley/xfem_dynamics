classdef engine_quadrature
    properties (Hidden)
        FEM
        subint_tri
        x_iso = [-1,-1; 1,-1; 1,1; -1,1]
        quad
        tri
        surf
    end

    methods
        %----------------------------------------------------------------------
        % Constructor.
        %----------------------------------------------------------------------
        function SINT = engine_quadrature(FEM,ngp,subint_tri,tri_ngp,srf_ngp)
            SINT.FEM = FEM;
            SINT.subint_tri = subint_tri;
            disp('Generating quad points...');
            SINT.quad = element_quad(ngp);
            SINT.tri  = element_tri(tri_ngp);
            SINT.surf = element_surf(srf_ngp);
            disp('Done generating quad points.');
        end

        %----------------------------------------------------------------------
        % Find the new subcell integration points/weights.
        %----------------------------------------------------------------------
        function domain = quadpoints(SINT,domain)
            % TODO - technically the 1d elements can be cut with XFEM too.
            if(domain.surfaces)
                for(e=1:domain.surf.ne)
                    domain.surf.element(e) = SINT.surf;
                end
            end
            disp('Subdividing XFEM elements for integration...');
            subdivided_elements = [];
            for(e=1:domain.ne)
                X = domain.gather(domain.X,e);
                gp = []; gw = [];
                elm = SINT.nodal_element(X);     % Determine the type of element.
                % If the element isn't cut, return regular element quadpoints.
                % If the element is cut, split into subelements and calculate new gps.
                if(e==1) domain.element    = elm;
                else     domain.element(e) = elm;
                end
                for(c=1:size(domain.sdf,2))
                    sdf.f = domain.gather(domain.sdf(c).f,e);
                    sdf.g = domain.gather(domain.sdf(c).g,e);
                    if(sum(sdf.g>0)>1 && (sum(sdf.f<0)~=elm.nne && sum(sdf.f<0)~=0))
                        subdivided_elements = [subdivided_elements, e];
                        [sub,xic] = SINT.subdivide(sdf,X,elm);
                        % Convert the gauss points into the coordinates of the fullcell.
                        for(sc=1:length(sub))
                            elm = SINT.nodal_element(sub(sc).X);
                            for(q=1:size(elm.gp,1))
                                ip = elm.gp(q,:);
                                % Evaluate using subcell coords. in fullcell isoparm. coordinate system.
                                detJ = det(SINT.FEM.J(elm,sub(sc).X,ip));
                                w = elm.gw(q) * detJ;
                                % Convert the gp from subcell isoparm. coords to fullcell isoparm. coords.
                                p = elm.N(ip) * sub(sc).X;
                                gp = [gp; p];
                                gw = [gw, w];
                            end
                        end
                        domain.element(e) = element_xfem(SINT.quad,X,sdf.f,gp,gw,xic,domain.interfaces);

                        % If interface energy is considered, add an XFEM interface element.
                        if(domain.interfaces)
                            Xc(1,:) = elm.N(xic(1,:)) * X;
                            Xc(2,:) = elm.N(xic(2,:)) * X;
                            domain.intr = domain.add_interface(Xc,SINT.surf,e);
                        end
                    end
                end
            end
            fprintf('\tNumber of Subdivided Elements: %d.\n',length(subdivided_elements));
            disp(strcat(sprintf('\tSubdivided Elements:'),sprintf(' %d,',subdivided_elements)));
            if(domain.interfaces)
                fprintf('\tNumber of Interface Elements: %d.\n',domain.intr.ne);
            end
            disp('Done subdividing elements.');
        end

        %----------------------------------------------------------------------
        % Determine the type of element.
        %----------------------------------------------------------------------
        function elm = nodal_element(SINT,X)
            if    (size(X,1) == 2) elm = SINT.surf;
            elseif(size(X,1) == 3) elm = SINT.tri;
            elseif(size(X,1) == 4) elm = SINT.quad;
            else error('Unknown element type.');
            end
        end

        %----------------------------------------------------------------------
        % Subdivide the element into subcells around the crack.
        % Use triangles if subint_tri==1
        %----------------------------------------------------------------------
        function [sub,xic] = subdivide(SINT,sdf,X,elm)
            if(sum(sdf.f==0) > 0)
                disp(X);
                error('Crack intersects node.');
            end
            % Corner cut by crack.
            if(sum(sdf.f<0)==1 || sum(sdf.f<0)==3)
                % Find the single node.
                for(n=1:length(sdf.f))
                    if((sdf.f(n)<0 && sum(sdf.f<0)==1) || ...
                            (sdf.f(n)>0 && sum(sdf.f>0)==1))
                        a=n;
                    end
                end
                % Add one for 1-based indexing.
                b=mod(a,4)+1; c=mod(a+1,4)+1; d=mod(a+2,4)+1;
                xic(1,:) = SINT.ls_intersection(a,b,X,sdf,elm);
                xic(2,:) = SINT.ls_intersection(a,d,X,sdf,elm);

                % Create the subcells.
                if(SINT.subint_tri==1)
                    sub(1).X = [SINT.x_iso(a,:); xic(1,:);        xic(2,:)];
                    sub(2).X = [SINT.x_iso(c,:); SINT.x_iso(d,:); xic(2,:)];
                else
                    sub(1).X = [SINT.x_iso(a,:); xic(1,:);        xic(1,:); xic(2,:)];
                    sub(2).X = [SINT.x_iso(c,:); SINT.x_iso(d,:); xic(2,:); xic(2,:)];
                end
                sub(3).X = [xic(2,:); xic(1,:); SINT.x_iso(b,:); SINT.x_iso(c,:)];
                % Element split in two.
            elseif(sum(sdf.f<0)==2)
                if(sdf.f(1)>0 && sdf.f(2)<0 || sdf.f(1)<0 && sdf.f(2)>0)
                    a=1; b=2; c=3; d=4;
                else
                    a=2; b=3; c=4; d=1;
                end
                xic(1,:) = SINT.ls_intersection(a,b,X,sdf,elm);
                xic(2,:) = SINT.ls_intersection(c,d,X,sdf,elm);

                sub(1).X = [SINT.x_iso(a,:); xic(1,:);        xic(2,:); SINT.x_iso(d,:)  ];
                sub(2).X = [SINT.x_iso(b,:); SINT.x_iso(c,:); xic(2,:); xic(1,:)         ];
                % Element not cut by crack.
            else
                disp('Incorrectly determined XFEM element. Not subdividing.');
                sub(1).X = X;
                xic = [];
            end
        end

        %----------------------------------------------------------------------
        % Simple integration of a function f over the element e.
        %----------------------------------------------------------------------
        function int = integrate(SINT,domain,e,f)
            element = domain.element(e);
            X = domain.gather(domain.X,e);
            int = 0.0;
            for(q=1:length(element.gp))
                x = element.gp(q,:);
                w = element.gw(q) * det(SINT.FEM.J(element,X,x));
                int = int + w * f(domain,e,x);
            end
        end

        %----------------------------------------------------------------------
        % Calculate the intersection of f(x)=0 on segment ab.
        %----------------------------------------------------------------------
        function xic = ls_intersection(SINT,a,b,X,sdf,elm)
            Xc = X(a,:) + (X(b,:)-X(a,:))*(sdf.f(a)/(sdf.f(a)-sdf.f(b)));
            xic = SINT.FEM.point_in_element(elm,X,Xc);
        end
    end
end


