function subcell_quadrature
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Author:   Philip Moseley
    %               Philip.Moseley@u.northwestern.edu
    %
    % Usage:    Calculate and display integration points for an element
    %           cut by a crack. Integrates a (continuous) function over
    %           the domain.
    %
    % License:  Academic Free License v3.0. You may modify and distribute
    %           the code as you please, as long as you maintain the
    %           attribution notices.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    old_path = path; addpath(genpath('.'));

    %------------------------Input--------------------------
    %----- General Options.
    E = 144;            % Elastic modulus.
    nu = 0.18;          % Poisson's ratio.
    rho = 11.3751;      % Density.

    ngp = 2;            % Number of quadrature points per dimension.
    tri_ngp = 4;        % Number of gauss points (total) for triagles.
    subint_tri = true;  % Whether or not to subintegrate using triangles.

    %----- Function to be integrated.
    % f = @(domain,e,x) 1;                        % Simple area.
    % f = @(domain,e,x) 2*(1-x(1)) + 3*(1-x(2));  % Random function.
    f = @force_at_ip;                           % Dynamic FEM forces.

    %----- Positions of nodes in reference global coords.
    mesh.X = [10,10; 15,10; 15,15; 10,15] + random('unif',-1,1,4,2);
    mesh.conn = [1 2 3 4];

    %----- Deformed coordinates (for stress calculations).
    x = mesh.X + random('unif',-1,1,4,2);

    %----- Crack points.
    crack = random('unif',10,15,2,2);   % Random crack.
    % crack = [0,0; 20,18];             % Crack across one corner (probably).
    % crack = [10,0; 15,25];            % Crack in half (probably).


    %-----------------------Prepare-------------------------
    % Ensure the endpoints of the crack are outside the element.
    cv = 20*diff(crack);
    crack(1,:) = crack(1,:) - cv;
    crack(2,:) = crack(2,:) + cv;

    % Prepare the system.
    FEM = engine_fem(E,nu,rho);
    LS = engine_ls();
    SINT = engine_quadrature(FEM,ngp,subint_tri,tri_ngp,1);
    domain = create_domain(mesh.X,mesh.conn,false,false,false);
    domain.x = x;
    domain.sdf = LS.sdf(crack,domain);


    %----------------------Calculate------------------------
    % Calculate the new quadpoints.
    domain.element = element_quad(2*ngp);
    xdomain        = SINT.quadpoints(domain);
    % Integrate.
    integral.full  = SINT.integrate(domain, 1,f);
    integral.sub   = SINT.integrate(xdomain,1,f);


    %--------------------Postprocessing---------------------
    nf = norm(integral.full);
    ns = norm(integral.sub);
    disp(sprintf('Full Area:       %f',domain.element.area(domain.gather(domain.X,1))));
    disp(sprintf('Integrated F:    %f',nf));
    disp(sprintf('Subintegrated F: %f',ns));
    disp(sprintf('Error norm:      %g',(nf-ns)/nf));
    plot_subcells(xdomain,1,crack);


    path(old_path);


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Helper Functions.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculate the force at an integration point.
    function [f] = force_at_ip(domain,e,x)
        element = domain.element(e);
        [F,dNdX,dPdX] = FEM.deformation_gradient(domain,e,x);
        f = -FEM.PK2(F) * FEM.B0(element,F,dNdX);
    end
end
