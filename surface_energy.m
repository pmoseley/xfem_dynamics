function domain = surface_energy(varargin)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Author:   Philip Moseley
    %               Philip.Moseley@u.northwestern.edu
    %
    % Usage:    Calculates the static FE response to a system under load.
    %           Uses XFEM to model up to one material interface without
    %           remeshing, and includes surface effects on mesh exteriors.
    %
    % License:  Academic Free License v3.0. You may modify and distribute
    %           the code as you please, as long as you maintain the
    %           attribution notices.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    old_path = path; addpath(genpath('.'));

    %------------------------Input--------------------------
    %----- Material Options. m1 is on the + side of levelset, m2 is on the - side.
    %       Positive is to the left in the interface direction.
    % Elastic moduli and Poisson's ratios.
    E = [168 200];      % Ni, Pt [GPa]. farsad2010extended.
    nu = [0.31 0.38];   % Ni, Pt. farsad2010extended.

    % Bulk constitutive laws (choose one).
    % Cb_function = @(E,nu) E/(1-nu^2)*[1 nu 0; nu 1 0; 0 0 (1-nu)/2];    % Plane stress.
    Cb_function = @(E,nu) E/((1+nu)*(1-2*nu))*[1-nu nu 0; nu 1-nu 0; 0 0 (1-2*nu)/2];   % Plane strain.

    % Surface constitutive laws.
    Ss{1} = [6.23  -4.72   0.0;         % Ni [J/m2], farsad2010extended.
             -4.72 6.23    0.0;
             0.0   0.0     0.93];
    Ss{2} = [13.25 9.30    0.0;         % Pt [J/m2], farsad2010extended.
             9.30  13.25   0.0;
             0.0   0.0     5.12];
    % Residual surface stresses (voigt).
    ts0{1} = [1.37; 1.37; 0.0];         % Ni [J/m2], farsad2010extended.
    ts0{2} = [2.52; 2.52; 0.0];         % Pt [J/m2], farsad2010extended.

    % Interface constitutive laws.
    Si = [9.34  -1.98   0.0;            % Ni/Pt [J/m2], farsad2010extended.
          -1.98 9.34    0.0;
          0.0   0.0     -4.94];
    % Residual interface stresses (voigt).
    ti0 = [0.34; 0.34; 0.0];            % Ni/Pt [J/m2], farsad2010extended.


    %----- General Options.
    surfaces = true;            % Include surface effects.
    interfaces = true;          % Include interface effects.
    stiffness = true;           % Include surface/interface stiffening effects (only disable for deflection plots).
    alpha = mean(E) * 1.0e8;    % Penalty factor for enforcing essential BCs.
    solver = 'bicg';            % bicg, direct, etc.
    fig.scale = 1.0e0;          % Factor to visibly scale the displaced mesh.
    fig.xfem = false;           % Plot the XFEM surfaces.
    fig.ref = true;             % Plot reference mesh underneath displaced mesh.
    fig.spy = false;            % Plot the sparsity graph.

    %----- Abaqus mesh file.
    mesh.scale = 5.0e1;         % Scale the mesh for size-effect studies.
    % mesh.file = './meshes/xfem_simple.inp';     % Simple xfem test, 16 element square.
    % mesh.file = './meshes/two_holes.inp';
    mesh.file = './meshes/beam.inp';        % Beam test.

    %----- Interface. Only one allowed, and must cross domain.
    interface = [];                     % No interface.
    % interface = mesh.scale * [-2.5, 0.0; 2.5, 0.0];
    interface = mesh.scale * [0.0,0.1; 1.0,0.1];

    %----- Boundary Conditions, one per row.
    % Use -inf or inf to indicate min or max coordinate of a dimension.
    bc = [];
    %-- Coordinates of points fixed in both x and y.
    nny = 7;
    bc.fixed = mesh.scale * horzcat(zeros(8,1), [0:0.2/nny:0.2]');
    %-- Displacements in X or Y, [x coord, displ] or [y coord, displ]
    % bc.ux = mesh.scale * [-inf, 0.0];
    % bc.uy = mesh.scale * [-inf, 0.0];
    %-- Forces in X or Y, [x coord, force] or [y coord, force]
    % bc.fy = mesh.scale * [inf, 10.0];

    %----- Integrator Options.
    ngp = 2;            % Number of gauss points per dimension.
    tri_ngp = 4;        % Number of gauss points (total) for triangles.
    srf_ngp = 2;        % Number of gauss points on surfaces and interfaces.
    subint_tri = true;  % Whether or not to subintegrate using triangles.



    %-----------------------Preprocessing-------------------------
    domain = create_domain(mesh,true,surfaces,interfaces);
    FEM  = engine_fem(E,nu,0.0);
    LS   = engine_ls();
    SINT = engine_quadrature(FEM,ngp,subint_tri,tri_ngp,srf_ngp);
    domain.sdf = LS.sdf(interface,domain);      % Calculate the sdf at every node.
    domain = SINT.quadpoints(domain);           % Calculate the new quadpoints.

    % Create the output figure.
    figure(); title('XFEM Implicit Solution with Surface Energy');
    fig.axis = gca; fig.handle = gcf;



    %----------------------Calculate------------------------
    for(e=1:domain.ne)
        element = domain.element(e);
        X = domain.gather(domain.X,e);
        ke = zeros(2*domain.nne);
        pe = zeros(2*domain.nne,1);
        if(element.xfem)
            kuq= zeros(2*domain.nne); kqq= zeros(2*domain.nne);
            qe = zeros(2*domain.nne,1);
        end

        % Develop the bulk element stiffness and force matrices.
        for(q=1:length(element.gp))
            x = element.gp(q,:);
            mat = determine_material(domain,e,x);
            Cb = Cb_function(E(mat),nu(mat));

            % This works because the current config == reference config, so F == I.
            % See Nonlinear FE for Continua and Structures page 203.
            [F,dNdX,dPdX] = FEM.deformation_gradient(domain,e,x);
            B = FEM.B0(element,F,dNdX);
            detJ = det(FEM.J(element,X,x));
            ke = ke + element.gw(q) * detJ * (B'*Cb*B);

            if(element.xfem)
                Beta = FEM.B0(element,F,dPdX);
                kuq = kuq + element.gw(q) * detJ * (B'*Cb*Beta);
                kqq = kqq + element.gw(q) * detJ * (Beta'*Cb*Beta);
            end
        end

        % Develop the surface element stiffness and force matrices.
        if(domain.surfaces)
            for(se=domain.surf.map{e})
                selement = domain.surf.element(se);
                sX = domain.gatherS(domain.X,se);

                % Project the material response onto the surface.
                Mp = calc_projection(sX);
                len = norm(diff(sX));
                detJ = 0.5*len;     % Needs 1d J, integrating 1d element.
                for(q=1:length(selement.gp))
                    x = selement.gp(q,:);
                    bx = local_1d_to_2d(FEM,element,X,selement,sX,x);
                    mat = determine_material(domain,e,bx);
                    Cs = Mp' * (Mp'*Ss{mat}*Mp) * Mp;
                    Ts = Mp' * ts0{mat};

                    [F,dNdX,dPdX] = FEM.deformation_gradient(domain,e,bx);
                    B = FEM.B0(element,F,dNdX);
                    if(stiffness)
                        ke = ke + selement.gw(q) * detJ * (B'*Cs*B);
                    end
                    pe = pe - selement.gw(q) * detJ * (B'*Ts);
                    % TODO - surface elements can be cut by XFEM.
                end
            end
        end

        % Develop the interface element stiffness and force matrices.
        if(domain.interfaces)
            for(se=domain.intr.map{e})
                selement = domain.intr.element(se);
                sX = domain.gatherI(domain.X,se);

                % Project the material response onto the surface.
                Mp = calc_projection(sX);
                Ci = Mp' * (Mp'*Si*Mp) * Mp;
                Ti = Mp' * ti0;

                len = norm(diff(sX));
                detJ = 0.5*len;     % Needs 1d J, integrating 1d element.
                for(q=1:length(selement.gp))
                    x = selement.gp(q,:);
                    bx = local_1d_to_2d(FEM,element,X,selement,sX,x);
                    [F,dNdX,dPdX] = FEM.deformation_gradient(domain,e,bx);
                    B = FEM.B0(element,F,dNdX);
                    Beta = FEM.B0(element,F,dPdX);
                    if(stiffness)
                        kuq = kuq + selement.gw(q) * detJ * (B'*Ci*Beta);
                        kqq = kqq + selement.gw(q) * detJ * (Beta'*Ci*Beta);
                    end
                    qe = qe - selement.gw(q) * detJ * (Beta'*Ti);
                end
            end
        end

        % Scatter into the full matrices.
        domain.K = domain.scatter(domain.K,ke,e);
        domain.f = domain.scatter(domain.f,pe,e);
        if(element.xfem)
            domain.Kuq = domain.scatter(domain.Kuq,kuq,e);
            domain.Kqq = domain.scatter(domain.Kqq,kqq,e);
            domain.xfem.q = domain.scatter(domain.xfem.q,qe,e);
        end
    end

    % Apply the boundary conditions.
    domain = apply_bcs(domain,bc,fig,false,alpha);

    % Build and solve the system of equations.
    [domain,idxs] = strip_matrix(domain);
    domain.K = [domain.K domain.Kuq; domain.Kuq' domain.Kqq];
    domain.f = [domain.f; domain.xfem.q];
    domain.x = implicit_solver(domain.K,domain.f,solver);

    domain.xfem.x(idxs) = domain.x(domain.nn+1:end,:);
    domain.x = domain.X + domain.x(1:domain.nn,:);

    % Plot the results.
    plot_disp_mesh(domain,bc,interface,fig);
    if(fig.ref) plot_surf_points(domain,fig); end
    if(fig.spy) plot_sparsity(domain); end
    print_disp_magnitude(domain,fig);
    plot_deflection(domain,E,[ts0{1}(1),ts0{2}(1),ti0(1)],mesh.scale*0.1);
    path(old_path);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper Functions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculate the projection operators for the surface.
    function [Mp] = calc_projection(X)
        V = diff(X);                    % Vector defining linear element.
        N = [V(2) -V(1)] / norm(V);     % Normal, nodes must be in CCW order.
        P = eye(2) - kron(N,N');        % Projection operator.
        Mp = [P(1,1)^2          P(1,2)^2            P(1,1)*P(1,2);
              P(1,2)^2          P(2,2)^2            P(2,2)*P(1,2);
              2*P(1,1)*P(1,2)   2*P(2,2)*P(1,2)     P(1,2)^2+P(1,1)*P(2,2)];
    end

    % Calculate the local coordinates in a 2d element given a point on a
    % corresponding 1d surface element.
    function [bx] = local_1d_to_2d(FEM,bulk_elm,bX,surf_elm,sX,x1d)
        V = diff(sX);                               % Vector defining linear element.
        len = norm(V);                              % Length of linear element.
        gx1d = surf_elm.N(x1d) * [0; len];          % Global coords in 1d.
        gx = sX(1,:) + gx1d*V;                      % Global coords in 2d.
        bx = FEM.point_in_element(bulk_elm,bX,gx);  % Local coords in 2d.
    end

    % Choose the constitutive law based on location relative to interface.
    function [mat] = determine_material(domain,e,bx)
        sdf = domain.gather(domain.sdf(1).f,e); % Assumes only 1 interface...
        N = element.N(bx);
        mat = (N*sdf'>0) + 1;
    end

    % Create the minimal system of equations by stripping out empty rows/columns.
    % This allows us to use full-size matrices for easy indexing, but still solve
    % the smaller system. We save "idxs" in order to expand the results back to
    % the full matrices for indexing again.
    function [domain,idxs] = strip_matrix(domain)
        idxs = any(domain.Kuq);
        if(isequal(idxs,any(domain.Kqq),any(domain.Kqq,2)))
            error('Problem with stripping XFEM matrix.');
        end
        domain.Kuq = domain.Kuq(:,idxs);        % Extract cols with a non-0.
        domain.Kqq = domain.Kqq(:,idxs);        % Extract cols with a non-0.
        domain.Kqq = domain.Kqq(idxs,:);        % Extract rows with a non-0.
        domain.xfem.q = domain.xfem.q(idxs);    % Extract rows with a non-0.
        idxs = reshape_voigt(idxs);
    end

    % Plot points to represent the identified surfaces.
    function [] = plot_surf_points(domain,fig)
        X = [];
        for(e=1:domain.ne)
            element = domain.element(e);
            if(domain.surfaces)
                for(se=domain.surf.map{e})
                    X = [X; domain.gatherS(domain.X,se)];
                end
            end
            if(domain.interfaces)
                for(se=domain.intr.map{e})
                    X = [X; domain.gatherI(domain.intr.X,se)];
                end
            end
        end
        if(~isequal(size(X),[0 0]))
            hold(fig.axis,'on'); plot(X(:,1),X(:,2),'hr'); hold(fig.axis,'off');
        end
    end
end

