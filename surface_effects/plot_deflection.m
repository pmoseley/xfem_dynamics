% Plot the deflection of a beam along the line midy.
function [nd,ed] = plot_deflection(domain,E,tau,midy)
    % Numerical Solution.
    nd = [];
    for(e=1:domain.ne)
        X = domain.gather(domain.X,e);
        midpt = mean(X);
        if(abs(midpt(2)-midy) < 1e-6)
            x = domain.gather(domain.x,e);
            nd = [nd; midpt(1) mean(x(:,2))];
        end
    end
    nd = sortrows(nd);
    plotX = nd(:,1);
    nd = nd(:,2) - midy;

    % Exact Solution.
    ed = exact_deflection(domain,E,tau,plotX,nd);

    %Plotting.
    figure;
    plot(plotX,nd,'-o',plotX,ed,'-o')
    legend('Numerical Deflection','Exact Deflection');
    title('Deflection of Beam');
    xlabel('Distance from Fixed Edge');
    ylabel('Deflection');
end

% Calculate the analytic solution for beam deflection.
% NOTE - Assumes surfaces have no effect on stiffness.
function [ed] = exact_deflection(domain,E,tau,plotX,nd)
    dim = max(domain.X)-min(domain.X);
    b = 1.0;            % Thickness in Z of beam.

    x = dim(2)*(3+E(2)/E(1)) / (4*(1+E(2)/E(1)));
    M = (tau(1)*dim(1))*(dim(2)-x) - tau(2)*dim(1)*x + (tau(3)*dim(1))*(0.5*dim(2)-x);
    I = dim(2)*b^3/12;
    inv_rho = M/(E(1)*I);
    ed = 0.5 * inv_rho * plotX.^2;

    % Fit exact solution to numerical solution.
    midX = floor(length(plotX)/2);
    b = nd(midX)/ed(midX);
    fprintf('Analytic deflection assumes beam thickness: %f\n',1/b^(1/3));
    ed = ed .* b;
end
