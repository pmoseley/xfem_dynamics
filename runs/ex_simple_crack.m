%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Small mesh, cracked completely in half horizontally.
% Boundary conditions pull down on bottom edge.
%
% Watch as the upper half of the mesh distorts when using mass
% lumping. In the exact (consistent) solution, this does not occur.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------------------Input--------------------------
%----- General Options.
ui.E = 144;            % Elastic modulus.
ui.nu = 0.18;          % Poisson's ratio.
ui.rho = 11.3751;      % Density.
ui.steps = 500;        % Number of simulation steps.
ui.os = 5;             % Output steps (how often to plot).
ui.fig.ref = true;     % Plot reference mesh underneath displaced mesh.

%----- Integrator Options.
ui.dt = 0.005;         % Delta time per step.
ui.ngp = 2;            % Number of gauss points per dimension.
ui.tint_type = 0;      % Type of time integrator. 0 = velocity verlet, 1 = newmark-beta.
ui.subint_tri = true;  % Whether or not to subintegrate using triangles.
ui.tri_ngp = 4;        % Number of gauss points (total) for triangles.

%----- Abaqus mesh file.
ui.mesh_file = '../meshes/xfem_simple.inp';     % Simple xfem test, 16 element square.

%----- Crack endpoints.
ui.crack = [-2.0, 0.0; 2.0, 0.0];  % Cracked down the middle.

%----- Boundary Conditions.
ui.bc.vy = [-inf, -0.5]; % Pulling down on bottom edge.

%----- Mass Matrix Options.
% ui.mass_type=1; ui.lump_non_xfem=0; ui.lump_xfem=0; %---> Exact solution.
% ui.mass_type=1; ui.lump_non_xfem=1; ui.lump_xfem=0; %---> Exact XFEM solution, common FEM lumping.
ui.mass_type=0; ui.ignore_muq=1;                    %---> Next best. Some forces across crack (used in DEBDM).
% ui.mass_type=2;                                     %---> Poor in general.

old_path = path; addpath(genpath('..'));
xfem_dynamics(ui);
path(old_path);
