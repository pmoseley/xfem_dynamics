--------------AUTHOR-----------------------------------
Philip Moseley
PhD Student, Belytschko Research Group
Northwestern University
Philip.Moseley@u.northwestern.edu

--------------LICENSE----------------------------------
This code is licensed under the Academic Free License v3.0. You may modify and distribute the code as you please, as long as you maintain the attribution notices.

--------------USAGE------------------------------------
xfem_explicit runs a dynamic xfem simulation. Currently this runs in plane strain.

xfem_implicit runs a linear implicit xfem simulation.

subcell_quadrature is simply for visualizing how the subcell integration scheme works.

All user configuration is in xfem_explicit.m, xfem_implicit.m, and subcell_quadrature.m. The files are well-commented, edit them as needed and run them from the matlab commandline. Alternately, run the graphical user interface by running gui.m. Alternately, run the pre-written examples in the runs directory.

--------------REFERENCES-------------------------------
P.M. Areias and T. Belytschko. A comment on the article ”a finite element method for simulation of strong
and weak discontinuities in solid mechanics” by a. hansbo. and p. hansbo commentary. Computer Methods in
Applied Mechanics and Engineering, 195(9-12):1275–1276, 2006.

T. Belytschko, R. Gracie, and G. Ventura. A review of extended/generalized finite element methods for material
modeling. Modelling and Simulation in Materials Science and Engineering, 17:043001, 2009.

T. Elguedj, A. Gravouil, and H. Maigre. An explicit dynamics extended finite element method. part 1: Mass
lumping for arbitrary enrichment functions. Computer Methods in Applied Mechanics and Engineering, 198(30-
32):2297–2317, 2009.

A. Gravouil, T. Elguedj, and H. Maigre. An explicit dynamics extended finite element method. part 2: Element-
by-element stable-explicit/explicit dynamic scheme. Computer Methods in Applied Mechanics and Engineering,
198(30-32):2318–2328, 2009.

T. Menouillard, J. Rethore, N. Moes, A. Combescure, and H. Bung. Mass lumping strategies for x-fem explicit dynamics: Application to crack propagation. International Journal for Numerical Methods in Engineering,
74(3):447–474, 2008.

G. Ventura, R. Gracie, and T. Belytschko. Fast integration and weight function blending in the extended finite
element method. International Journal for Numerical Methods in Engineering, 77(1):1–29, 2009.

